export const MEGA_MENU_DATA = {
    'categoryList' : [
        {
            "uid": "Mg==",
            "name": "Default Category",
            "children": [
                {
                    "uid": "Ng==",
                    "include_in_menu": 1,
                    "name": "Our Services",
                    "position": 1,
                    "url_path": "our-services",
                    "children": [],
                    "__typename": "CategoryTree"
                },
                {
                    "uid": "Nw==",
                    "include_in_menu": 1,
                    "name": "E-Commerce",
                    "position": 2,
                    "url_path": "e-commerce",
                    "children": [
                        {
                            "uid": "OA==",
                            "include_in_menu": 1,
                            "name": "Magento",
                            "position": 1,
                            "url_path": "magento-development",
                            "children": [],
                            "__typename": "CategoryTree"
                        },
                        {
                            "uid": "OQ==",
                            "include_in_menu": 1,
                            "name": "SuiteCommerce",
                            "position": 5,
                            "url_path": "netsuite-suitecommerce",
                            "children": [],
                            "__typename": "CategoryTree"
                        },
                        {
                            "uid": "OQ==woo",
                            "include_in_menu": 1,
                            "name": "WooCommerce",
                            "position": 10,
                            "url_path": "woocommerce-development",
                            "children": [],
                            "__typename": "CategoryTree"
                        },
                        {
                            "uid": "OQ==shpi",
                            "include_in_menu": 1,
                            "name": "Shopify",
                            "position": 15,
                            "url_path": "shopify-development",
                            "children": [],
                            "__typename": "CategoryTree"
                        }
                    ],
                    "__typename": "CategoryTree"
                },
                {
                    "uid": "Ng==join",
                    "include_in_menu": 1,
                    "name": "Join Us",
                    "position": 5,
                    "url_path": "join-us",
                    "children": [],
                    "__typename": "CategoryTree"
                },
                {
                    "uid": "Ng==contact",
                    "include_in_menu": 1,
                    "name": "Contact Us",
                    "position": 20,
                    "url_path": "contact-us",
                    "children": [],
                    "__typename": "CategoryTree"
                }
            ],
            "__typename": "CategoryTree"
        }
    ]
};
