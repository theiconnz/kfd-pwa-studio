/* eslint-disable */
/**
 * Custom interceptors for the project.
 *
 * This project has a section in its package.json:
 *    "pwa-studio": {
 *        "targets": {
 *            "intercept": "./local-intercept.js"
 *        }
 *    }
 *
 * This instructs Buildpack to invoke this file during the intercept phase,
 * as the very last intercept to run.
 *
 * A project can intercept targets from any of its dependencies. In a project
 * with many customizations, this function would tap those targets and add
 * or modify functionality from its dependencies.
 */
const moduleOverridePlugin = require('./moduleOverrideWebpackPlugin');
const componentOverrideMapping = {
    '@magento/venia-ui/lib/components/Footer/footer.js': './src/components/Footer/footer.js',
    '@magento/venia-ui/lib/components/Header/header.js': './src/components/Header/header.js',
    '@magento/venia-ui/lib/components/Logo/logo.js': './src/components/Logo/logo.js',
    '@magento/peregrine/lib/talons/MegaMenu/useMegaMenu.js': './src/lib/talons/MegaMenu/useMegaMenu.js',
    '@magento/peregrine/lib/talons/Adapter/useAdapter.js': './src/peregrine/lib/talons/Adapter/useAdapter.js'
};


function localIntercept(targets) {
    targets.of('@magento/pwa-buildpack').transformUpward.tap(def => {
        def.staticFromRoot.inline.body.file.template.inline =
            './public/{{ filename }}';
    });


    targets.of('@magento/pwa-buildpack').webpackCompiler.tap(compiler => {
        // registers our own overwrite plugin for webpack
        new moduleOverridePlugin(componentOverrideMapping).apply(compiler);
    })
}

module.exports = (targets) = localIntercept;

